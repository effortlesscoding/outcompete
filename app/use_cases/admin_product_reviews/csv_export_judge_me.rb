module AdminProductReviews
  class CsvExportJudgeMe
    include UseCase

    attr_accessor :product, :csv

    def perform(product_id, shopify_links)
      @product = Product.find(product_id)
      @shopify_links = shopify_links
      create_csv
      self
    end

    def create_csv
      @csv = CSV.generate( encoding: 'Windows-1251' ) do |csv|
        csv << [
          'title',
          'body',
          'rating',
          'review_date',
          'reviewer_name',
          'reviewer_email',
          'product_id',
          'product_handle',
          'reply',
          'picture_urls'
        ]
        @shopify_links.each do |shopify_link|
          @product.product_reviews.each do |review|
            review_images_sources = review.product_review_images.reduce('') do |text, review_image|
              if text.empty?
                text += review_image.image_url 
              else
                text += ",#{review_image.image_url}"
              end
            end
            csv << [
              review.review_title,
              review.review_body,
              review.rating,
              review.date,
              review.reviewer,
              review.reviewer_email,
              shopify_link[:product_id],
              shopify_link[:product_handle],
              '',
              review_images_sources
            ]
          end
        end
      end
    end
  end
end