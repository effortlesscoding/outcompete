module AdminForms
  class CreateNewSupplierProduct
    include UseCase
    attr_reader :params, :product, :supplier, :supplier_product

    def perform(params)
      @params = params
      ActiveRecord::Base.transaction do
        process_product if errors.none?
        process_supplier if errors.none?
        process_supplier_product if errors.none?
      end
      self
    end

    private

    def process_product
      if new_product?
        @product = Product.create!(
          name: params[:new_product_name],
          image_url: params[:new_product_image_url],
          code: params[:new_product_code],
          product_category_id: params[:new_product_category]
        )
        errors.messages.merge!(@product.errors.messages)
        errors.add(:base, "Product could not be created") if @product.nil?
      elsif old_product?
        product_id = params[:product_id]
        @product = Product.find(product_id)
        errors.add(:base, "Product #{product_id} not found") if @product.nil?
      else
        errors.add(:base, 'product_type unknown')
      end
    rescue => e
      errors.add(:base, "Product could not be created #{e.message}")
      raise ActiveRecord::Rollback
    end

    def new_product?
      params[:product_type] == 'new'
    end

    def old_product?
      params[:product_type] == 'old'
    end

    def process_supplier
      if new_supplier?
        @supplier = Supplier.create!(
          name: params[:new_supplier_name],
          profile_url: params[:new_supplier_profile_url],
          supplier_source_id: params[:new_supplier_source],
          location_id: params[:new_supplier_location]
        )
        errors.add(:base, "Supplier could not be created") if @supplier.nil?
      elsif old_supplier?
        supplier_id = params[:supplier_id]
        @supplier = Supplier.find(supplier_id)
        errors.add(:base, "Supplier #{supplier_id} not found") if @supplier.nil?
      else
        errors.add(:base, 'supplier_type unknown')
      end
    rescue => e
      errors.add(:base, "Supplier could not be created #{e.message}")
      raise ActiveRecord::Rollback
    end

    def new_supplier?
      params[:supplier_type] == 'new'
    end

    def old_supplier?
      params[:supplier_type] == 'old'
    end

    def process_supplier_product
      @supplier_product = SupplierProduct.create!(
        name: params[:supplier_product_name],
        image_url: params[:supplier_product_image_url],
        reference_url: params[:supplier_product_reference_url],
        product_id: @product.id,
        supplier_id: @supplier.id
      )
      errors.add(:base, "Supplier product could not be created") if @supplier_product.nil?
    rescue
      raise ActiveRecord::Rollback
    end
  end
end