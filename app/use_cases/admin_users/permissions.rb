module AdminUsers
  class Permissions
    class << self
      def can_access?(user, action)
        case action

        # Admin Users resource
        when :admin_users_create
          return user.superadmin?
        when :admin_users_index
          return user.superadmin?
        when :admin_users_edit
          return user.superadmin?
        when :admin_users_update
          return user.superadmin?
        when :admin_users_destroy
          return user.superadmin?
        when :admin_users_show
          return user.superadmin?

        # Competitor Product Intel
        when :competitor_product_intel_create
          return user.superadmin? || user.research_staff?
        when :competitor_product_intel_index
          return user.superadmin? || user.research_staff?
        when :competitor_product_intel_destroy
          return user.superadmin?
        when :competitor_product_intel_edit
          return user.superadmin? || user.research_staff?
        when :competitor_product_intel_update
          return user.superadmin? || user.research_staff?
        when :competitor_product_intel_show
          return user.superadmin? || user.research_staff?

        # Contact Source
        when :contact_source_create
          return user.superadmin?
        when :contact_source_destroy
          return user.superadmin?
        when :contact_source_index
          return user.superadmin?
        when :contact_source_edit
          return user.superadmin?
        when :contact_source_update
          return user.superadmin?
        when :contact_source_show
          return user.superadmin?

        # Currency Source
        when :currency_create
          return user.superadmin?
        when :currency_index
          return user.superadmin?
        when :currency_edit
          return user.superadmin?
        when :currency_update
          return user.superadmin?
        when :currency_destroy
          return user.superadmin?
        when :currency_show
          return user.superadmin?

        # Location resource
        when :location_create
          return user.superadmin?
        when :location_destroy
          return user.superadmin?
        when :location_edit
          return user.superadmin?
        when :location_index
          return user.superadmin?
        when :location_show
          return user.superadmin?
        when :location_update
          return user.superadmin?

        # Product resource
        when :product_create
          return user.superadmin? || user.research_staff? || user.sales_staff?
        when :product_destroy
          return user.superadmin?
        when :product_edit
          return user.superadmin? || user.research_staff? || user.sales_staff?
        when :product_index
          return true
        when :product_show
          return true
        when :product_update
          return user.superadmin? || user.research_staff? || user.sales_staff?
        when :product_action_add_competitor
          return user.superadmin? || user.research_staff? || user.sales_staff?
        when :product_action_add_listing
          return user.superadmin? || user.sales_staff?
        when :product_action_add_supplier
          return user.superadmin? || user.negotiation_staff?
        when :product_action_add_review
          return user.superadmin? || user.sales_staff?
        when :product_view_competitors
          return true
        when :product_view_reviews
          return user.superadmin? || user.sales_staff?
        when :product_view_listings
          return user.superadmin? || user.sales_staff?
        when :product_view_suppliers
          return user.superadmin? || user.negotiation_staff? || user.sales_staff?

        # Product Listing resource
        when :product_listing_create
          return user.superadmin? || user.sales_staff?
        when :product_listing_destroy
          return user.superadmin?
        when :product_listing_edit
          return user.superadmin? || user.sales_staff?
        when :product_listing_index
          return user.superadmin? || user.sales_staff?
        when :product_listing_show
          return user.superadmin? || user.sales_staff?
        when :product_listing_update
          return user.superadmin? || user.sales_staff?
        when :product_listing_publish
          return user.superadmin? || user.sales_staff?

        # Sales Channel resource
        when :sales_channel_create
          return user.superadmin?
        when :sales_channel_destroy
          return user.superadmin?
        when :sales_channel_edit
          return user.superadmin?
        when :sales_channel_index
          return user.superadmin?
        when :sales_channel_show
          return user.superadmin?
        when :sales_channel_update
          return user.superadmin?

        # Script resource
        when :script_create
          return user.superadmin?
        when :script_destroy
          return user.superadmin?
        when :script_edit
          return user.superadmin?
        when :script_index
          return user.superadmin?
        when :script_show
          return user.superadmin?
        when :script_update
          return user.superadmin?

        # Shipping Destination resource
        when :shipping_destination_create
          return user.superadmin?
        when :shipping_destination_destroy
          return user.superadmin?
        when :shipping_destination_edit
          return user.superadmin?
        when :shipping_destination_index
          return user.superadmin?
        when :shipping_destination_show
          return user.superadmin?
        when :shipping_destination_update
          return user.superadmin?

        # Supplier
        when :supplier_create
          return user.superadmin? || user.negotiation_staff?
        when :supplier_destroy
          return user.superadmin?
        when :supplier_edit
          return user.superadmin? || user.negotiation_staff?
        when :supplier_index
          return user.superadmin? || user.negotiation_staff?
        when :supplier_show
          return user.superadmin? || user.negotiation_staff?
        when :supplier_update
          return user.superadmin? || user.negotiation_staff?

        # Supplier Contact
        when :supplier_contact_create
          return user.superadmin? || user.negotiation_staff?
        when :supplier_contact_destroy
          return user.superadmin?
        when :supplier_contact_edit
          return user.superadmin? || user.negotiation_staff?
        when :supplier_contact_index
          return user.superadmin? || user.negotiation_staff?
        when :supplier_contact_show
          return user.superadmin? || user.negotiation_staff?
        when :supplier_contact_update
          return user.superadmin? || user.negotiation_staff?

        # Supplier Product
        when :supplier_product_create
          return user.superadmin? || user.negotiation_staff? || user.sales_staff?
        when :supplier_product_destroy
          return user.superadmin?
        when :supplier_product_edit
          return user.superadmin? || user.negotiation_staff? || user.sales_staff?
        when :supplier_product_index
          return user.superadmin? || user.negotiation_staff?
        when :supplier_product_show
          return user.superadmin? || user.negotiation_staff? || user.sales_staff?
        when :supplier_product_update
          return user.superadmin? || user.negotiation_staff? || user.sales_staff?

        # Supplier Product Quote
        when :supplier_product_quote_create
          return user.superadmin? || user.negotiation_staff? || user.sales_staff?
        when :supplier_product_quote_destroy
          return user.superadmin?
        when :supplier_product_quote_edit
          return user.superadmin? || user.negotiation_staff? || user.sales_staff?
        when :supplier_product_quote_index
          return user.superadmin? || user.negotiation_staff?
        when :supplier_product_quote_show
          return user.superadmin? || user.negotiation_staff? || user.sales_staff?
        when :supplier_product_quote_update
          return user.superadmin? || user.negotiation_staff? || user.sales_staff?

        # Supplier Product Shipment
        when :supplier_product_shipment_create
          return user.superadmin? || user.negotiation_staff?
        when :supplier_product_shipment_destroy
          return user.superadmin?
        when :supplier_product_shipment_edit
          return user.superadmin? || user.negotiation_staff?
        when :supplier_product_shipment_index
          return user.superadmin? || user.negotiation_staff?
        when :supplier_product_shipment_show
          return user.superadmin? || user.negotiation_staff?
        when :supplier_product_shipment_update
          return user.superadmin? || user.negotiation_staff?

        # Supplier Source
        when :supplier_source_create
          return user.superadmin? || user.sales_staff?
        when :supplier_source_destroy
          return user.superadmin?
        when :supplier_source_edit
          return user.superadmin? || user.sales_staff?
        when :supplier_source_index
          return user.superadmin?
        when :supplier_source_show
          return user.superadmin?
        when :supplier_source_update
          return user.superadmin?

        when :shipping_provider_create
          return user.superadmin?
        when :shipping_provider_destroy
          return user.superadmin?
        when :shipping_provider_edit
          return user.superadmin?
        when :shipping_provider_index
          return user.superadmin?
        when :shipping_provider_show
          return user.superadmin?
        when :shipping_provider_update
          return user.superadmin?

        when :product_variation_index
          return user.superadmin?
        # PAGES

        # Suppliers Products Orders Management
        when :suppliers_products_orders_management
          return user.superadmin?
        when :supplier_product_variation_index
          return user.superadmin?

        when :new_supplier_product
          return user.superadmin? || user.negotiation_staff? || user.sales_staff?

        when :new_supplier_product_quote
          return user.superadmin? || user.negotiation_staff? || user.sales_staff?

        when :new_product_mega_form
          return user.superadmin? || user.sales_staff?
        end
        return user.superadmin?
      end
    end
  end
end
