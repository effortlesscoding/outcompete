ActiveAdmin.register SupplierProduct do
  
  menu parent: 'Data Management', :if => proc{ AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_index) }

  controller do
    def raise_error
      raise ActionController::RoutingError.new('Not Found')
    end

    # TODO - Get all actions
    def create
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_create)
      super
    end

    def index
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_index)
      super
    end

    def edit
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_edit)
      super
    end

    def update
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_update)
      super
    end

    def destroy
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_destroy)
      super
    end

    def show
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_show)
      super
    end
  end

  action_item :add_quote do
    begin
      link_to 'Add a quote', admin_new_supplier_product_quote_path(supplier_product_id: supplier_product.id), method: :get
    rescue => e
      puts e.message
    end
  end

  action_item :add_variation do
    begin
      link_to 'Add a Variation', new_admin_supplier_product_variation_path(supplier_product_variation: { supplier_product_id: supplier_product.id }), method: :get
    rescue => e
      puts e.message
    end
  end

  permit_params :product_id, :supplier_id, :name, :reference_url,
                :image_url, :description, :quantity_in_stock

  index do
    column :image do |p|
      image_tag p.image_url, class: 'col_image-5'
    end
    column :name
    column :description
    column :url do |p|
      link_to 'Link', p.reference_url
    end
    actions
  end
  show do
    attributes_table do
      row :name
      row :image_url do |product|
        if product.image_url.present? && product.image_url.starts_with?('http')
          image_tag product.image_url, class: 'col_image-5'
        else
          ''
        end
      end
      row :description
      row :product
      row :supplier
      row :reference_url do |product|
        link_to 'External Link', product.reference_url if product.reference_url.present?
      end
    end

    panel 'Variations' do
      table_for supplier_product.supplier_product_variations do
        column :product_variation
        column :value
      end
    end

    panel 'Quotes' do
      table_for supplier_product.supplier_product_quotes do
        column :contact { |quote| quote.supplier_contact.reference_number }
        column :contact_source { |quote| quote.supplier_contact.contact_type }
        column :quantity
        column :unit_price
        column :shipping_price
        column :unit_cost_aud { |quote| quote.cost_per_unit(Currency.find_by(name: 'AUD')) }
        column :total_price
        column :total_price_aud do |quote|
          quote.total_price_in_currency(Currency.find_by(name: 'AUD'))
        end
        column :edit { |quote| link_to 'Edit', edit_admin_supplier_product_quote_path(quote.id) } 
      end
    end
  end

end
