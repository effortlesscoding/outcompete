ActiveAdmin.register SupplierContact do
  menu parent: 'Data Management', :if => proc{ AdminUsers::Permissions.can_access?(current_admin_user, :supplier_contact_index) }

  controller do
    def raise_error
      raise ActionController::RoutingError.new('Not Found')
    end

    # TODO - Get all actions
    def create
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_contact_create)
      super
    end

    def index
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_contact_index)
      super
    end

    def edit
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_contact_edit)
      super
    end

    def update
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_contact_update)
      super
    end

    def destroy
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_contact_destroy)
      super
    end

    def show
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_contact_show)
      super
    end
  end

  permit_params :name, :supplier_id, :contact_type, :reference_number
  
  index do
    column :name
    column :contact_source
    column :contact_type
    column :reference_number
    actions
  end
end
