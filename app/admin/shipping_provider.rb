ActiveAdmin.register ShippingProvider do
    menu parent: 'Config', :if => proc{ AdminUsers::Permissions.can_access?(current_admin_user, :shipping_provider_index) }

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
    permit_params :name
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

end
