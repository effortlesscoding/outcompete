ActiveAdmin.register_page "New Supplier Product Quote" do
  menu if: proc { false }
  content do
    # your content
    return unless AdminUsers::Permissions.can_access?(current_admin_user, :new_supplier_product_quote)
    supplier_product = SupplierProduct.find(params[:supplier_product_id])
    supplier_contacts = supplier_product.supplier.supplier_contacts
    quote_states = SupplierProductQuote.aasm.states
    currencies = Currency.all
    render partial: 'new_supplier_product_quote', locals: {
      supplier_product: supplier_product,
      supplier_contacts: supplier_contacts,
      quote_states: quote_states,
      currencies: currencies
    }
  end
end
