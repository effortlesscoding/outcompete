ActiveAdmin.register SupplierProductQuote do
  menu parent: 'Data Management', :if => proc{ AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_quote_index) }

  controller do
    def raise_error
      raise ActionController::RoutingError.new('Not Found')
    end

    # TODO - Get all actions
    def create
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_quote_create)
      super
    end

    def index
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_quote_index)
      super
    end

    def edit
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_quote_edit)
      super
    end

    def update
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_quote_update)
      super
    end

    def destroy
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_quote_destroy)
      super
    end

    def show
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_quote_show)
      super
    end
  end

  scope :all, default: true
  scope :not_shipped
  scope :negotiating
  scope :waiting_for_tracking_number
  scope :waiting_for_shipment
  scope :shipped

  permit_params :supplier_product_id, :supplier_contact_id, :supplier_product_shipment_id,
                :quantity, :state, :total_shipping_cents,
                :unit_price_cents, :currency_id, :shipping_provider_id

  action_item :add_shipment, only: [:show], if: proc { !supplier_product_quote.has_shipment? } do
    link_to 'Add shipment', new_admin_supplier_product_shipment_path(supplier_product_quote_id: supplier_product_quote.id), method: :get
  end

  # TODO: Maybe it is better to have 1:1 relationship
  action_item :update_shipment, only: [:show], if: proc { supplier_product_quote.has_shipment? } do
    link_to 'Update shipment', admin_supplier_product_shipment_path(supplier_product_quote.supplier_product_shipment_id), method: :get
  end

  index do
    column :supplier_product
    column :state do |quote|
      status_tag quote.state
    end
    column :quantity
    column :shipping_price
    column :unit_price
    column :total_price
    column :actions do |resource|
      link_to 'View', admin_supplier_product_quote_path(resource.id)
    end
  end

  show do
    attributes_table do
      row :main_product do |obj|
        link_to obj.supplier_product.product.name, admin_product_path(obj.supplier_product.product.id)
      end
      row :supplier_product
      row :supplier_contact
      row :supplier_contact_type do |obj|
        obj.supplier_contact.contact_type
      end
      row :supplier_contact_reference do |obj|
        obj.supplier_contact.reference_number
      end
      row :state do |obj|
        status_tag obj.state
      end
      row :quantity
      row :total_shipping_cents
      row :unit_price_cents
      row :currency
    end

    panel 'Shipments' do
      if supplier_product_quote.has_shipment?
        attributes_table_for supplier_product_quote.supplier_product_shipment do
          row :shipping_provider
          row :tracking_number
          row :tracking_url
          row :shipping_destination
          row :shipping_address do |quote|
            quote.shipping_destination.address
          end
        end
      end
    end
  end
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

end
