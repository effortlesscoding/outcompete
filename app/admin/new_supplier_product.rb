ActiveAdmin.register_page "New Supplier Product" do
  menu if: proc { false }
  content do
    # your content
    return unless AdminUsers::Permissions.can_access?(current_admin_user, :new_supplier_product)
    product_id = params[:product_id]
    product = Product.find(product_id)
    suppliers = Supplier.all
    render partial: 'new_supplier_product', locals: { product: product, suppliers: suppliers }
  end
end
