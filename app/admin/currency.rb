ActiveAdmin.register Currency do
  menu parent: 'Config', :if => proc{ AdminUsers::Permissions.can_access?(current_admin_user, :currency_index) }

  controller do
    def raise_error
      raise ActionController::RoutingError.new('Not Found')
    end

    def create
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :currency_create)
      super
    end

    def index
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :currency_index)
      super
    end

    def edit
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :currency_edit)
      super
    end

    def update
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :currency_update)
      super
    end

    def destroy
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :currency_destroy)
      super
    end

    def show
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :currency_show)
      super
    end
  end
  
  permit_params :name
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  
  class << self
    def default
      find_by(name: 'AUD')
    end
  end
end
