ActiveAdmin.register Product do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters

  scope :all, default: true
  scope :considering
  scope :searching_for_suppliers
  scope :negotiating
  scope :ordering
  scope :waiting_for_tracking_number
  scope :waiting_for_shipping
  scope :fully_stocked
  scope :dropshipping
  scope :selling

  menu parent: 'Data Management', if: proc { AdminUsers::Permissions.can_access?(current_admin_user, :product_index) }

  controller do

    def raise_error
      raise ActionController::RoutingError.new('Not Found')
    end

    def create
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :product_create)
      super
    end

    def index
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :product_index)
      super
    end

    def edit
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :product_edit)
      super
    end

    def update
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :product_update)
      super
    end

    def destroy
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :product_destroy)
      super
    end

    def show
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :product_show)
      super
    end
  end

  permit_params :name, :image_url, :state, :marketing_materials_url, :code, :state,
                :weight_gram, :width_mm, :height_mm, :depth_mm, :shipping_cost_cents,
                :product_category_id


  collection_action :autocomplete, method: :get do
    term = params[:term] || ''
    products = Product.where("LOWER(name) LIKE '%#{term.downcase}%'")
    if products.count > 0
      render json: products, each_serializer: ProductAutocompleteSerializer
    else
      render json: [{id: 0, label: 'No matching product found'}]
    end
  end

  collection_action :csv_export_judgeme, method: :post do
    # Need to work on this one here... Hmmm.
    shopify_links = []
    params.require(:shopify_links).permit!.to_h.each do |key, value|
      shopify_links << {
        product_id: value["'product_id'"],
        product_handle: value["'product_handle'"]
      }
    end
    csv_export = AdminProductReviews::CsvExportJudgeMe.new.perform(params[:product_id], shopify_links)
    csv = csv_export.csv
    product = csv_export.product
    send_data csv, type: 'text/csv; charset=windows-1251; header=present', disposition: "attachment; filename=export__#{product.name}.csv"
  end

  action_item :add_competitor, only: [:show], if: proc { product.present? && AdminUsers::Permissions.can_access?(current_admin_user, :product_action_add_competitor) } do
    link_to 'Add a competitor', new_admin_competitor_product_intel_path(competitor_product_intel: { product_id: product.id, intel_date: DateTime.now }), method: :get
  end

  action_item :add_listing, only: [:show], if: proc { product.present? && AdminUsers::Permissions.can_access?(current_admin_user, :product_action_add_listing) } do
    link_to 'Add a Listing', new_admin_product_listing_path(product_listing: { product_id: product.id, start_date: DateTime.now }), method: :get
  end

  action_item :add_supplier, only: [:show], if: proc { product.present? && AdminUsers::Permissions.can_access?(current_admin_user, :product_action_add_supplier) } do
    link_to 'Add a Supplier Product', new_admin_supplier_product_path(supplier_product: { product_id: product.id }), method: :get
  end

  action_item :add_review, only: [:show], if: proc { product.present? && AdminUsers::Permissions.can_access?(current_admin_user, :product_action_add_review) } do
    link_to 'Add a Product Review', new_admin_product_review_path(product_review: { product_id: product.id, date: DateTime.now }), method: :get
  end
  action_item :judge_me_csv_export, only: [:show], if: proc { product.present? && AdminUsers::Permissions.can_access?(current_admin_user, :product_action_judgme_csv_export) } do
    link_to 'Judge.Me CSV Export', admin_csv_export_judge_me_path(product_id: product.id), method: :get
  end

  show do
    attributes_table do
      row :image do |product|
        image_tag product.image_url, class: 'col_image'
      end
      row :name
      row :code
      row :state do |product|
        status_tag product.state
      end
      row :dimensions do |product|
        product.dimensions_text(Product.dimensions[:cm])
      end
      row :weight do |product|
        product.weight_text(Product.weight_units[:kg])
      end
      row :approximate_unit_cost do |product|
        product.approximate_unit_cost_text(Currency::AUD)
      end
      row :approximate_domestic_shipping_cost do |product|
        product.approximate_domestic_shipping_cost_text(Currency::AUD)
      end
      row :marketing_materials_url do |product|
        link_to 'Marketing Materials Link', product.marketing_materials_url if product.marketing_materials_url.present?
      end
      row :competition_score { |p| status_tag p.competition_score }
      row :stockmovement_score { |p| status_tag p.stockmovement_score }
    end
    if AdminUsers::Permissions.can_access?(current_admin_user, :product_view_competitors)
      panel 'Competitors' do
        table_for product.competitor_product_intels do
          column :image do |competitor|
            image_tag competitor.image_url, class: 'col_image'
          end
          column :location
          column :unit_price
          column :shipping_price
          column :source
          column :sales_per_month
          column :total_entries
          column :edit do |competitor|
            link_to 'View/Edit', edit_admin_competitor_product_intel_path(competitor.id)
          end
          column :view do |competitor|
            link_to 'External Link', competitor.url, target: '_blank'
          end
        end
      end
    end
    if AdminUsers::Permissions.can_access?(current_admin_user, :product_view_listings)
      panel 'Listings' do
        table_for product.product_listings do
          column :sales_channel do |listing|
            listing.sales_channel.name
          end
          column :name
          column :url do |listing|
            if listing.url.nil? || listing.url.empty?
              'N/A'
            else
              link_to 'View External Listing', listing.url, method: :get, target: :blank
            end
          end
          column :listing_price
          column :listing_shipping_price { |l| l.shipping_price_text }
          column :product_cost { |l| l.product_cost_text }
          column :payment_gateway_fees { |l| l.payment_gateway_fees_text }
          column :sales_channel_unit_fees { |l| l.sales_channel_unit_fees_text }
          column :shippings_cost do |listing|
            listing.shippings_cost_text
          end
          column :approximate_margin do |listing|
            listing.approximate_margin_text
          end
          column :state { |l| status_tag l.state }
          column :publish do |listing|
            if !listing.published? && AdminUsers::Permissions.can_access?(current_admin_user, :product_listing_publish)
              link_to 'Publish to the Sales Channel', create_remote_listing_admin_product_listings_path(product_id: product.id, listing_id: listing.id), method: :post
            else
              ''
            end
          end
          column :view do |listing|
            link_to 'Edit', admin_product_listing_path(listing.id), method: :get
          end
        end
      end
    end

    if AdminUsers::Permissions.can_access?(current_admin_user, :product_view_suppliers)
      panel 'Suppliers' do
        table_for product.supplier_products do
          column :name do |supplier_product|
            panel supplier_product.supplier.name do
              table_for supplier_product do
                column :image do |supplier_product|
                  if supplier_product.image_url.present? && supplier_product.image_url.starts_with?('http')
                    image_tag supplier_product.image_url, class: 'col_image-5'
                  else
                    'N/A'
                  end
                end
                column :name
                column :source do |supplier_product|
                  supplier_product.supplier.source
                end
                column :external_link do |supplier_product|
                  link_to 'External Link', supplier_product.reference_url, target: :blank
                end
                column :view do |supplier_product|
                  link_to 'View', admin_supplier_product_path(supplier_product.id)
                end
                column :add_quote do |supplier_product|
                  link_to 'Add a quote', admin_new_supplier_product_quote_path(supplier_product_id: supplier_product.id), method: :get
                end
              end
              table_for supplier_product.supplier_product_quotes do
                column :contact { |quote| quote.supplier_contact.reference_number }
                column :contact_source { |quote| quote.supplier_contact.contact_type }
                column :quantity
                column :unit_price
                column :shipping_price
                column :shipping_provider { |quote| quote.shipping_provider ? quote.shipping_provider.name : 'N/A' }
                column :unit_cost_usd { |quote| quote.cost_per_unit(Currency.find_by(name: 'USD')) }
                column :unit_cost_aud { |quote| quote.cost_per_unit(Currency.find_by(name: 'AUD')) }
                column :total_price
                column :total_price_aud do |quote|
                  quote.total_price_in_currency(Currency.find_by(name: 'AUD'))
                end
                column :state do |quote|
                  status_tag quote.state
                end
                column :edit { |quote| link_to 'Edit', edit_admin_supplier_product_quote_path(quote.id) }
              end
            end
          end
        end
      end
    end

    if AdminUsers::Permissions.can_access?(current_admin_user, :product_view_reviews)
      panel 'Reviews' do
        table_for product.product_reviews do
          column :reviewer
          column :reviewer_email
          column :review_title
          column :review_body { |review| simple_format review.review_body }
          column :rating
          column :external_link do |review|
            link_to 'External Link', review.source_url, target: :blank
          end
          column :images do |review|
            table_for review.product_review_images do
              column :_ { |review_image| image_tag review_image.image_url, class: 'col_image-5' }
            end
          end
          column :images { |review| link_to 'Add Image', new_admin_product_review_image_path(product_review_image: { product_review_id: review.id }), method: :get }
          column :edit { |review| link_to 'Edit', admin_product_review_path(review.id) }
        end
      end
    end
  end

  index do
    column :image do |product|
      image_tag product.image_url, class: 'col_image'
    end
    column :name
    column :code
    column :state do |product|
      status_tag product.state
    end
    column :product_category
    column :competition_score { |p| status_tag p.competition_score }
    column :created_at
    actions
  end
end
