ActiveAdmin.register ProductListing do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  menu parent: 'Data Management', :if => proc{ AdminUsers::Permissions.can_access?(current_admin_user, :product__listing_index) }
  permit_params :product_id, :sales_channel_id, :currency_id,
                :listing_costs_cents, :name, :url,
                :listing_price_cents, :shipping_price_cents,
                :start_date, :end_date, :state

  collection_action :create_remote_listing, method: :post do
    listing = ProductListing.find(params[:listing_id])
    if !listing.shopify?
      redirect_to admin_product_path(params[:product_id]), flash: { error: "Listing failed to publish because it is not a Shopify sales channel" }
    elsif listing.published?
      redirect_to admin_product_path(params[:product_id]), flash: { error: "Listing failed to publish because it is already published" }
    elsif listing.start_publishing!
      # TODO:
      # 1) Move to a use case
      # 2) Make it more flexible with different shopify stores
      product = Product.find(params[:product_id])
      new_product = ShopifyAPI::Product.new
      new_product.variants = [
        { price: "#{listing.listing_dollar_price}" }
      ]
      pricing_label = "#{listing.listing_price}_#{listing.shipping_price}Shipping"
      # CatsLove's Cats Hidden Promotions
      # ConversionLabs's default collection
      # new_product.collection_id = 3554574362
      new_product.product_type = pricing_label
      new_product.title = listing.name
      new_product.tags = []
      new_product.tags << product.code
      new_product.tags << 'SATISFACTION_GUARANTEED'
      new_product.tags << 'FREE_SHIPPING' if listing.shipping_price_cents.zero?

      if listing.end_date.present?
        formatted_date = listing.end_date.strftime('%Y-%m-%dT%H:%M:%S%z')
        new_product.tags << "LIMITED_TIME_PROMOTION_#{formatted_date}"
      end
      if new_product.save
        listing.url = "#{listing.sales_channel.url}/admin/products/#{new_product.id}"
        if listing.finish_publishing! && listing.save!
          redirect_to admin_product_path(params[:product_id]), flash: { notice: 'Listing has been published.' }
        else
          listing.error_message = listing.errors.full_messages.join(',')
          listing.fail!
          redirect_to admin_product_path(params[:product_id]), flash: { error: "Listing has been published but FAILED to save remotely. #{listing.error_message}" }
        end
      else
        listing.error_message = new_product.errors.full_messages.join(',')
        listing.fail!
        redirect_to admin_product_path(params[:product_id]), flash: { error: "Listing failed to publish #{listing.error_message}" }
      end
    else
      redirect_to admin_product_path(params[:product_id]), flash: { error: "Listing failed to publish #{listing.errors.full_messages.join(', ')}." }
    end
  end

  controller do
    def raise_error
      raise ActionController::RoutingError.new('Not Found')
    end

    # TODO - Get all actions
    def create
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :product_listing_create)
      super
    end

    def index
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :product_listing_index)
      super
    end

    def edit
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :product_listing_edit)
      super
    end

    def update
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :product_listing_update)
      super
    end

    def destroy
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :product_listing_destroy)
      super
    end

    def show
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :product_listing_show)
      super
    end
  end

  form do |f|
    f.object.currency = Currency::USD
    f.object.listing_price_cents = 0 if f.object.listing_price_cents.nil?
    f.object.shipping_price_cents = 0 if f.object.shipping_price_cents.nil?
    f.inputs do
      f.input :product
      f.input :sales_channel
      f.input :name
      f.input :currency
      f.input :listing_price_cents
      f.input :shipping_price_cents
      f.input :start_date
      f.input :end_date
      f.input :state
    end
    actions
  end
end
