ActiveAdmin.register_page 'Suppliers Products Orders Management' do
  menu if: proc { false }
  content title: 'Product Suppliers' do
    return unless AdminUsers::Permissions.can_access?(current_admin_user, :suppliers_products_orders_management)
    product = Product.find(params[:product_id])
    currency_usd = Currency.find_by(name: 'USD')
    currency_aud = Currency.find_by(name: 'AUD')
    render partial: 'suppliers_products_orders_management', locals: { product: product, currency_usd: currency_usd, currency_aud: currency_aud }
  end
end
