ActiveAdmin.register_page "New Product Mega Form" do
  menu parent: 'Data Entry'
  content do
    # your content
    return unless AdminUsers::Permissions.can_access?(current_admin_user, :new_product_mega_form)
    render partial: 'new_product_mega_form'
  end

  page_action :commit, method: :post do
    return unless AdminUsers::Permissions.can_access?(current_admin_user, :new_product_mega_form)
    pars = params.permit(:supplier_details)
    creation = AdminForms::CreateNewSupplierProduct.new.perform(
      params.permit(
        :new_product_image_url,
        :new_product_name,
        :new_supplier_location,
        :new_supplier_name,
        :new_supplier_profile_url,
        :new_supplier_source,
        :new_product_code,
        :new_product_category,
        :product_search_details,
        :product_id,
        :product_type,
        :supplier_search_details,
        :supplier_id,
        :supplier_product_image_url,
        :supplier_product_name,
        :supplier_product_reference_url,
        :supplier_type
      )
    )
    if creation.errors.any?
      render partial: 'new_product_mega_form_error', locals: { errors: creation.errors }
    else
      redirect_to admin_product_path(creation.product), flash: { notice: 'Supplier product has been created successfully.' }
    end
  end
end
