ActiveAdmin.register CompetitorProductIntel do
  menu parent: 'Data Management', if: proc{ AdminUsers::Permissions.can_access?(current_admin_user, :competitor_product_intel_index) }
  permit_params :product_id, :days_listed, :sales_per_day, :total_entries, # TODO: calculate sales per day...
                :shipping_price_cents, :total_sales, :unit_price_cents,
                :currency_id, :image_url, :item_location, :source, :url, :intel_date,
                :sales_channel_id, :location_id

  controller do
    def raise_error
      raise ActionController::RoutingError.new('Not Found')
    end
    
    def create
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :competitor_product_intel_create)
      super
    end

    def index
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :competitor_product_intel_index)
      super
    end

    def edit
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :competitor_product_intel_edit)
      super
    end

    def update
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :competitor_product_intel_update)
      super
    end

    def destroy
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :competitor_product_intel_destroy)
      super
    end

    def show
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :competitor_product_intel_show)
      super
    end
  end
  
  index do
    column :product
    column :image_url do |competitor|
      image_tag competitor.image_url, class: 'col_image-5'
    end
    column :source { |competitor| competitor.sales_channel.nil? ? 'N/A' : competitor.sales_channel.name }
    column :item_location { |competitor| competitor.location.nil? ? 'N/A' : competitor.location.name }
    column :days_listed
    column :sales_per_month { |competitor| competitor.sales_per_month.round(1) }
    column :unit_price
    column :shipping_price
    column :url do |competitor|
      link_to 'Link', competitor.url, class: 'col_link', target: '_blank'
    end
    actions
  end
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

end
