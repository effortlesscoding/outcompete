ActiveAdmin.register SupplierProductShipment do
  
  menu parent: 'Data Management', :if => proc{ AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_shipment_index) }

  controller do
    def raise_error
      raise ActionController::RoutingError.new('Not Found')
    end

    # TODO - Get all actions
    def create
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_shipment_create)
      super
    end

    def index
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_shipment_index)
      super
    end

    def edit
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_shipment_edit)
      super
    end

    def update
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_shipment_update)
      super
    end

    def destroy
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_shipment_destroy)
      super
    end

    def show
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_shipment_show)
      super
    end
  end

  permit_params :shipping_provider, :shipping_destination_id,
                :tracking_number, :tracking_url
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  controller do

    def edit
      @supplier_product_quote = nil
      super
    end

    def new
      @supplier_product_quote = nil
      if params.has_key?(:supplier_product_quote_id)
        @supplier_product_quote = SupplierProductQuote.find(params[:supplier_product_quote_id])
      end
      super
    end

    def create
      create! do |format|
        ActiveRecord::Base.transaction do
          shipment = SupplierProductShipment.create(permitted_params[:supplier_product_shipment])
          shipment.save.tap do |success|
            raise ActiveRecord::Rollback unless success
            if params.has_key?(:supplier_product_shipment) &&
               params[:supplier_product_shipment].has_key?(:supplier_product_quotes)
               quote_id = params[:supplier_product_shipment][:supplier_product_quotes]
               if quote_id.present?
                 quote = SupplierProductQuote.find(params[:supplier_product_shipment][:supplier_product_quotes])
                 quote.supplier_product_shipment = shipment
                 quote.save.tap do |success|
                    raise ActiveRecord::Rollback unless success
                    format.html { redirect_to admin_supplier_product_quote_path(quote.id) }
                 end
               end
            end
          end
          format.html { redirect_to admin_supplier_product_shipment_path(shipment.id) }
        end
      end
    end
  end

  form do |f|
    if f.supplier_product_quote.present?
      f.panel 'Shipment for a Product quote' do
        f.attributes_table_for f.supplier_product_quote do
          row :supplier_product do |quote|
            quote.supplier_product.name
          end
          row :quantity
          row :unit_price
          row :shipping_price
        end
      end
    end
    f.inputs 'Shipment Details' do
      f.input :supplier_product_quotes, as: :hidden, input_html: { value: f.supplier_product_quote.present? ? f.supplier_product_quote.id : '' }
      f.input :shipping_destination, as: :select, collection: ShippingDestination.shipping_options
      f.input :shipping_provider
      f.input :tracking_number
      f.input :tracking_url
    end
    actions
  end
end
