ActiveAdmin.register Script do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  permit_params :name, :description, :text
  menu parent: 'Config', :if => proc{ AdminUsers::Permissions.can_access?(current_admin_user, :script_index) }

  controller do
    def raise_error
      raise ActionController::RoutingError.new('Not Found')
    end

    # TODO - Get all actions
    def create
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :script_create)
      super
    end

    def index
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :script_index)
      super
    end

    def edit
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :script_edit)
      super
    end

    def update
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :script_update)
      super
    end

    def destroy
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :script_destroy)
      super
    end

    def show
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :script_show)
      super
    end
  end
  
  index do
    column :name
    column :description
    actions
  end

  show do
    attributes_table do
      row :name
      row :description
      row :text
    end
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :description
      f.input :text
    end
    actions
  end
end
