ActiveAdmin.register SupplierProductVariation do
  menu parent: 'Data Management', :if => proc{ AdminUsers::Permissions.can_access?(current_admin_user, :supplier_product_variation_index) }
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  permit_params :supplier_product_id, :product_variation_id, :value
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

end
