ActiveAdmin.register Supplier do
  
  menu parent: 'Data Management', :if => proc{ AdminUsers::Permissions.can_access?(current_admin_user, :supplier_index) }

  controller do
    def raise_error
      raise ActionController::RoutingError.new('Not Found')
    end

    # TODO - Get all actions
    def create
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_create)
      super
    end

    def index
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_index)
      super
    end

    def edit
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_edit)
      super
    end

    def update
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_update)
      super
    end

    def destroy
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_destroy)
      super
    end

    def show
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_show)
      super
    end
  end

  permit_params :name, :source, :location_id, :profile_url, :supplier_source_id

  action_item :add_contact, only: [:show], if: proc { supplier.present? } do
    link_to 'Add contact', new_admin_supplier_contact_path(supplier_contact: { supplier_id: supplier.id }), method: :get
  end
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  collection_action :autocomplete, method: :get do
    term = params[:term] || ''
    suppliers = Supplier.where("LOWER(name) LIKE '%#{term.downcase}%'")
    if suppliers.count > 0
      render json: suppliers, each_serializer: SupplierAutocompleteSerializer
    else
      render json: [{id: 0, label: 'No suppliers found'}]
    end
  end

  index do
    column :id do |supplier|
      link_to supplier.id, admin_supplier_path(supplier.id)
    end
    column :name
    column :profile_url do |supplier|
      link_to 'Link', supplier.profile_url, class: 'col_link', target: '_blank'
    end
    column :source
    column :location
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :location
      row :supplier_source
      row :profile_url do |supplier|
        link_to 'Link', supplier.profile_url, class: 'col_link', target: '_blank'
      end
    end
    
    panel 'Contacts' do
      table_for supplier.supplier_contacts do
        column :name
        column :contact_source
        column :reference_number
        column :view do |contact|
          link_to 'view', admin_supplier_contact_path(contact), method: :get
        end
        column :edit do |contact|
          link_to 'Edit', edit_admin_supplier_contact_path(contact), method: :get
        end
      end
    end

    panel 'All quotes' do
      table_for supplier.supplier_products do
        column :name
        column :image do |product|
          image_tag product.image_url, class: 'col_image'
        end
      end
    end
  end
end
