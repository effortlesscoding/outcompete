ActiveAdmin.register_page "Csv Export Judge Me" do
  menu if: proc { false }
  content do
    # your content
    return unless AdminUsers::Permissions.can_access?(current_admin_user, :csv_export_judge_me)
    render partial: 'csv_export_judge_me', locals: { product_id: params[:product_id] }
  end
end
