ActiveAdmin.register SupplierSource do
  menu parent: 'Config', :if => proc{ AdminUsers::Permissions.can_access?(current_admin_user, :supplier_source_index) }

  controller do
    def raise_error
      raise ActionController::RoutingError.new('Not Found')
    end

    # TODO - Get all actions
    def create
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_source_create)
      super
    end

    def index
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_source_index)
      super
    end

    def edit
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_source_edit)
      super
    end

    def update
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_source_update)
      super
    end

    def destroy
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_source_destroy)
      super
    end

    def show
      raise_error unless AdminUsers::Permissions.can_access?(current_admin_user, :supplier_source_show)
      super
    end
  end

  permit_params :name, :url
end
