ActiveAdmin.register ProductReview do
  menu parent: 'Data Management', if: proc { AdminUsers::Permissions.can_access?(current_admin_user, :product_reviews_index) }
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  permit_params :reviewer, :reviewer_email, :review_body, :review_title,
                :rating, :date, :source_url, :product_id
#
  action_item :add_listing, only: [:show], if: proc { AdminUsers::Permissions.can_access?(current_admin_user, :product_review_action_add_product_review_image) } do
    link_to 'Add a Review Image', new_admin_product_review_image_path(product_review_image: { product_review_id: product_review.id }), method: :get
  end
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  form do |f|
    f.inputs do
      f.input :reviewer
      f.input :reviewer_email
      f.input :review_title
      f.input :review_body, :as => :text
      f.input :rating
      f.input :date
      f.input :source_url
      f.input :product
    end
    actions
  end


  show do
    attributes_table do
      row :reviewer
      row :reviewer_email
      row :review_title
      row :review_body
      row :rating
      row :date
      row :source_url
      row :product
    end
    panel 'Images' do
      table_for product_review.product_review_images do
        column :review_image { |review_image| image_tag review_image.image_url, class: 'col_image' }
        column :edit { |review_image| link_to 'Edit', admin_product_review_image_path(review_image)}
      end
    end
  end
end
