ActiveAdmin.register ProductReviewImage do
  menu parent: 'Data Management', if: proc { AdminUsers::Permissions.can_access?(current_admin_user, :product_review_images_index) }

  permit_params :image_url, :product_review_id

end
