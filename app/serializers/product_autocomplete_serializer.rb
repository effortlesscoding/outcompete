class ProductAutocompleteSerializer < ActiveModel::Serializer
  attributes :id, :label
end