class SupplierAutocompleteSerializer < ActiveModel::Serializer
  attributes :id, :label
end