# == Schema Information
#
# Table name: supplier_products
#
#  id                :uuid             not null, primary key
#  name              :string
#  image_url         :string
#  description       :string
#  quantity_in_stock :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  product_id        :uuid
#  supplier_id       :uuid
#  reference_url     :string
#

class SupplierProduct < ApplicationRecord
  belongs_to :product
  belongs_to :supplier

  has_many :supplier_product_quotes
  has_many :supplier_product_variations

  # Validations
  validates :name, presence: true
end
