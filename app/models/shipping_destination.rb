# == Schema Information
#
# Table name: shipping_destinations
#
#  id         :uuid             not null, primary key
#  name       :string
#  phone      :string
#  address    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ShippingDestination < ApplicationRecord
  has_many :supplier_product_shipments

  class << self
    def shipping_options
      ShippingDestination.all.map do |s|
        ["#{s.name} - #{s.phone} - #{s.address}", s.id]
      end
    end
  end
end
