# == Schema Information
#
# Table name: product_review_images
#
#  id                :uuid             not null, primary key
#  image_url         :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  product_review_id :uuid
#

class ProductReviewImage < ApplicationRecord
  belongs_to :product_review
  
  validates :product_review, presence: true
  validates :image_url, presence: true
end
