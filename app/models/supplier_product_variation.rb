# == Schema Information
#
# Table name: supplier_product_variations
#
#  id                   :uuid             not null, primary key
#  value                :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  product_variation_id :uuid
#  supplier_product_id  :uuid
#

class SupplierProductVariation < ApplicationRecord
  belongs_to :product_variation
  belongs_to :supplier_product
end
