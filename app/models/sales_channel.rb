# == Schema Information
#
# Table name: sales_channels
#
#  id         :uuid             not null, primary key
#  name       :string
#  url        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SalesChannel < ApplicationRecord
  has_many :product_listings

  def shopify?
    return false if name.nil?
    name.downcase.include? 'shopify' 
  end

  def fees(product_price_cents)
    if name == 'eBay AU'
      product_price_cents * 0.08 # Fitness industry
    else
      0
    end
  end

  def payment_gateway_fees(product_price_cents)
    if name == 'eBay AU'
      product_price_cents * 0.026 + 0.3 # Fitness industry
    elsif name == 'Shopify'
      product_price_cents * 0.0175 + 0.3
    else
      0
    end
  end
end
