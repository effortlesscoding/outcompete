# == Schema Information
#
# Table name: supplier_sources
#
#  id         :uuid             not null, primary key
#  name       :string
#  url        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SupplierSource < ApplicationRecord
  def alibaba?
    name.downcase.include? 'alibaba'
  end

  def aliexpress?
    name.downcase.include? 'aliexpress'
  end
end
