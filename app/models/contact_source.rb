# == Schema Information
#
# Table name: contact_sources
#
#  id         :uuid             not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ContactSource < ApplicationRecord
  class << self
    def alibaba
      find_by(name: 'Alibaba Chat')
    end

    def aliexpress
      find_by(name: 'AliExpress Chat')
    end
  end
end
