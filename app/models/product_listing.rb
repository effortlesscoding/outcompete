# == Schema Information
#
# Table name: product_listings
#
#  id                   :uuid             not null, primary key
#  name                 :string
#  phone                :string
#  url                  :string
#  listing_costs_cents  :integer
#  listing_price_cents  :integer
#  start_date           :datetime
#  end_date             :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  currency_id          :uuid
#  sales_channel_id     :uuid
#  product_id           :uuid
#  shipping_price_cents :integer
#  state                :integer
#  error_message        :string
#

class ProductListing < ApplicationRecord
  include AASM
  belongs_to :product
  belongs_to :sales_channel
  belongs_to :currency

  validates :product, presence: true
  validates :sales_channel, presence: true
  validates :currency, presence: true


  enum state: {
    draft: 0,
    publishing: 1,
    published: 2,
    failed: 3
  }

  aasm column: :state, enum: true do
    state :draft, initial: true
    state :publishing
    state :published
    state :failed

    event :start_publishing do
      transitions from: :draft, to: :publishing
    end

    event :finish_publishing do
      transitions from: :publishing, to: :published
    end

    event :fail do
      transitions from: :publishing, to: :failed
    end
  end


  def currency_name
    currency.name
  end

  def approximate_margin_text
    Money.new(approximate_margin_cents, currency_name).format(symbol: currency_name + ' ')
  end

  def approximate_margin_cents
    return 0 if listing_price_cents.nil? ||
                product_cost_cents.nil? ||
                payment_gateway_fees_cents.nil? ||
                sales_channel_unit_fees_cents.nil?
    listing_shipping_price_cents = shipping_price_cents.nil? ? 0 : shipping_price_cents            
    listing_price_cents + listing_shipping_price_cents -
      product_cost_cents -
      payment_gateway_fees_cents -
      sales_channel_unit_fees_cents
  end

  # TODO: Shipping cost must also take a currency
  def product_cost_cents
    product.approximate_shipping_cost_cents(currency) +
      product.approximate_unit_cost_cents(currency)
  end

  def payment_gateway_fees_cents
    listing_shipping_price_cents = shipping_price_cents.nil? ? 0 : shipping_price_cents
    sales_channel.payment_gateway_fees(listing_price_cents + listing_shipping_price_cents)
  end

  def sales_channel_unit_fees_cents
    sales_channel.fees(listing_price_cents)
  end

  # Texts

  def shippings_cost_text
    product.shippings_cost_text(currency)
  end

  def product_cost_text
    product.product_unit_cost_text(currency)
  end

  def shipping_price_text
    return Money.new(shipping_price_cents, currency_name).format(symbol: currency_name + ' ') unless shipping_price_cents.nil?
    return 'N/A'
  end

  def payment_gateway_fees_text
    Money.new(-payment_gateway_fees_cents, currency_name).format(symbol: currency_name + ' ')
  end

  def sales_channel_unit_fees_text
    Money.new(-sales_channel_unit_fees_cents, currency_name).format(symbol: currency_name + ' ')
  end

  def listing_cost
    Money.new(-listing_costs_cents, currency_name).format(symbol: currency_name + ' ')
  end

  def shipping_price
    Money.new(shipping_price_cents, currency_name).format(symbol: currency_name + ' ')
  end

  def listing_price
    Money.new(listing_price_cents, currency_name).format(symbol: currency_name + ' ')
  end

  # Publishing

  def listing_dollar_price
    Money.new(listing_price_cents, 'USD').to_f
  end

  def shopify?
    sales_channel.shopify?
  end
end
