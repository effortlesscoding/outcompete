# == Schema Information
#
# Table name: supplier_product_shipments
#
#  id                      :uuid             not null, primary key
#  shipping_provider       :string
#  tracking_number         :string
#  tracking_url            :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  shipping_destination_id :uuid
#  shipping_provider_id    :uuid
#

class SupplierProductShipment < ApplicationRecord
  has_many :supplier_product_quotes
  belongs_to :shipping_destination
  belongs_to :shipping_provider
end
