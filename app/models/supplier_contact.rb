# == Schema Information
#
# Table name: supplier_contacts
#
#  id                :uuid             not null, primary key
#  name              :string
#  contact_type      :string
#  contact_id        :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  supplier_id       :uuid
#  reference_number  :string
#  contact_source_id :uuid
#

class SupplierContact < ApplicationRecord
  belongs_to :supplier
  belongs_to :contact_source
  has_many :supplier_product_quotes
  
  # Validations
  validates :name, presence: true
  validates :reference_number, presence: true
end
