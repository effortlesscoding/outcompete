# == Schema Information
#
# Table name: currencies
#
#  id         :uuid             not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Currency < ApplicationRecord
  AUD = find_by(name: 'AUD')
  USD = find_by(name: 'USD')
end
