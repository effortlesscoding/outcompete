# == Schema Information
#
# Table name: competitor_product_intels
#
#  id                   :uuid             not null, primary key
#  days_listed          :integer
#  shipping_price_cents :integer
#  total_sales          :integer
#  unit_price_cents     :integer
#  additional_info      :string
#  image_url            :string
#  url                  :string
#  intel_date           :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  product_id           :uuid
#  currency_id          :uuid
#  total_entries        :integer
#  location_id          :uuid
#  sales_channel_id     :uuid
#

class CompetitorProductIntel < ApplicationRecord
  belongs_to :product
  belongs_to :currency
  belongs_to :sales_channel
  belongs_to :location

  validates :days_listed, presence: true
  validates :total_sales, presence: true
  validates :total_entries, presence: true
  validates :currency, presence: true

  def currency_text
    currency.present? ? currency.name : 'GBP'
  end

  def sales_per_month
    (total_sales.to_f / days_listed.to_f) * 365 / 12
  end

  def sales_per_day
    total_sales.to_f / days_listed.to_f
  end

  def unit_price
    return 'N/A' if unit_price_cents.nil?
    Money.new(unit_price_cents, currency_text).format(symbol: currency_text + ' ')
  end

  def shipping_price
    return 'N/A' if shipping_price_cents.nil?
    Money.new(shipping_price_cents, currency_text).format(symbol: currency_text + ' ')
  end
end
