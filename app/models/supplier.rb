# == Schema Information
#
# Table name: suppliers
#
#  id                 :uuid             not null, primary key
#  name               :string
#  profile_url        :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  supplier_source_id :uuid
#  location_id        :uuid
#

class Supplier < ApplicationRecord
  after_create :create_default_supplier_contact, guard: :no_supplier_contact?

  has_many :supplier_contacts
  has_many :supplier_products

  belongs_to :supplier_source
  belongs_to :location

  # Validations
  validates :name, presence: true, uniqueness: { case_sensitive: true }
  validates :supplier_source, presence: true

  def label
    "#{id.to_s} -- #{name} -- #{source}"
  end

  def source
    supplier_source.name
  end

  def no_supplier_contact?
    supplier_contacts.nil? || supplier_contacts.empty? || supplier_contacts.count == 0
  end

  def create_default_supplier_contact
    if supplier_source.alibaba?
      contact = SupplierContact.new(
        supplier: self,
        name: 'Default',
        contact_source: ContactSource.alibaba,
        reference_number: supplier_source.name
      )
      contact.save.tap do |success|
        puts "Contact creation failed #{contact.errors.full_messages}" unless success
      end
    elsif supplier_source.aliexpress?
      contact = SupplierContact.new(
        supplier: self,
        name: 'Default',
        contact_source: ContactSource.aliexpress,
        reference_number: supplier_source.name
      )
      contact.save.tap do |success|
        puts "Contact creation failed #{contact.errors.full_messages}" unless success
      end
    end
  end
end
