# == Schema Information
#
# Table name: competitor_product_variations
#
#  id                   :uuid             not null, primary key
#  value                :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  product_variation_id :uuid
#

class CompetitorProductVariation < ApplicationRecord
end
