# == Schema Information
#
# Table name: products
#
#  id                      :uuid             not null, primary key
#  name                    :string
#  image_url               :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  state                   :integer
#  marketing_materials_url :string
#  width_mm                :integer
#  height_mm               :integer
#  depth_mm                :integer
#  weight_gram             :integer
#  shipping_cost_cents     :integer
#  code                    :string
#  product_category_id     :uuid
#

class Product < ApplicationRecord
  include AASM

  enum dimensions: {
    cm: 0,
    mm: 1,
    meters: 2
  }

  enum weight_units: {
    gram: 0,
    kg: 1
  }

  # States:
  enum state: {
    discarded: -1,
    considering: 0,
    searching_for_suppliers: 1,
    negotiating: 2,
    ordering: 10,
    waiting_for_tracking_number: 3,
    waiting_for_shipping: 4,
    fully_stocked: 5,
    dropshipping: 6,
    selling: 7
  }

  aasm column: :state, enum: true do
    state :discarded
    state :considering, initial: true
    state :searching_for_suppliers
    state :negotiating
    state :ordering
    state :waiting_for_tracking_number
    state :waiting_for_shipping
    state :received
    state :dropshipping
    state :selling
    state :fully_stocked

    event :discard do
      transitions to: :discarded
    end

    event :consider do
      transitions to: :considering
    end

    event :confirm do
      transitions to: :searching_for_suppliers
    end

    event :negotiate do
      transitions from: [:received, :discarded, :considering, :searching_for_suppliers], to: :negotiating
    end

    event :order do
      transitions from: [:received, :discarded, :considering, :negotiating], to: :ordering
    end

    event :pay do
      transitions from: :ordering, to: :waiting_for_tracking_number
    end

    event :wait_for_shipping do
      transitions from: :waiting_for_tracking_number, to: :waiting_for_shipping
    end

    event :receive do
      transitions from: :waiting_for_shipping, to: :received
    end
  end

  has_many :product_reviews
  has_many :product_listings
  has_many :competitor_product_intels
  has_many :supplier_products

  belongs_to :product_category

  # Validations
  validates :name, presence: true
  validates :code, presence: true, uniqueness: { case_sensitive: true }

  def label
    "#{id.to_s} -- #{name}"
  end

  def competition_score
    return 'N/A' if competitor_product_intels.nil? || competitor_product_intels.empty?
    competitor = competitor_product_intels.first
    return 'N/A' if competitor.total_entries.nil?
    return :not_competitive if competitor.total_entries < 30
    return :ok if competitor.total_entries >= 30 && competitor.total_entries  < 120
    return :competitive if competitor.total_entries >= 120 && competitor.total_entries  < 360
    return :very_competitive if competitor.total_entries >= 360 && competitor.total_entries  < 1200
    return :super_competitive if competitor.total_entries >= 1200
  end

  def stockmovement_score
    return 'N/A' if competitor_product_intels.nil? || competitor_product_intels.empty?
    competitor = competitor_product_intels.first
    return 'N/A' if competitor.sales_per_month.nil?
    return :slow if competitor.sales_per_month < 30
    return :ok if competitor.sales_per_month >= 30 && competitor.sales_per_month  < 120
    return :fast if competitor.sales_per_month >= 120 && competitor.sales_per_month  < 360
    return :very_fast if competitor.sales_per_month >= 360 && competitor.sales_per_month  < 1200
    return :super_fast if competitor.sales_per_month >= 1200
  end

  def height(unit = DIMENSIONS_MM)
    0 if height_mm.nil?
    case unit
    when Product.dimensions[:mm]
      height_mm
    when Product.dimensions[:cm]
      height_mm.to_f / 10
    when Product.dimensions[:meters]
      height_mm.to_f / 1000
    else
      -1
    end
  end

  def width(unit)
    0 if width_mm.nil?
    case unit
    when Product.dimensions[:mm]
      width_mm
    when Product.dimensions[:cm]
      width_mm.to_f / 10
    when Product.dimensions[:meters]
      width_mm.to_f / 1000
    else
      -1
    end
  end

  def depth(unit)
    0 if depth_mm.nil?
    case unit
    when Product.dimensions[:mm]
      depth_mm
    when Product.dimensions[:cm]
      depth_mm.to_f / 10
    when Product.dimensions[:meters]
      depth_mm.to_f / 1000
    else
      -1
    end
  end

  def weight_text(unit)
    0 if depth_mm.nil?
    case unit
    when Product.weight_units[:gram]
      "#{weight_gram}g"
    when Product.weight_units[:kg]
      "#{weight_gram.to_f / 1000}kg"
    else
      -1
    end
  end

  def dimensions_text(unit)
    "#{width(unit)} x #{height(unit)} x #{depth(unit)} #{Product.dimensions.key(unit)}^3"
  end

  def approximate_unit_cost_text(currency)
    Money.new(-approximate_unit_cost_cents(currency), currency.name)
         .format(symbol: currency.name + ' ')
  end

  # Assumptions - Using our own packagin
  def approximate_domestic_shipping_cost_text(currency)
    return 'N/A' if shipping_cost_cents.nil?
    Money.new(shipping_cost_cents, currency.name)
         .format(symbol: currency.name + ' ')
  end

  def shippings_cost_text(currency)
    Money.new(-approximate_shipping_cost_cents(currency), currency.name)
         .format(symbol: currency.name + ' ')
  end

  def product_unit_cost_text(currency)
    Money.new(-approximate_unit_cost_cents(currency).to_i, currency.name)
         .format(symbol: currency.name + ' ')
  end

  # TODO - Add conversions
  def approximate_shipping_cost_cents(currency)
    shipping_cost_cents || 0
  end

  def approximate_unit_cost_cents(currency)
    most_recent_quotes = []
    supplier_products.each do |supplier_product|
      quotes = supplier_product
               .supplier_product_quotes
               .where(state:
                [
                  SupplierProductQuote.states[:ordering],
                  SupplierProductQuote.states[:waiting_for_tracking_number],
                  SupplierProductQuote.states[:waiting_for_shipment],
                  SupplierProductQuote.states[:shipped],
                  SupplierProductQuote.states[:dropshipping]
                ])
               .order('created_at DESC')
      most_recent_quote = quotes.find { |q| q.state != SupplierProductQuote.states[:shipped] }
      most_recent_quote = quotes.first unless most_recent_quote.present?
      most_recent_quotes << most_recent_quote unless most_recent_quote.nil?
    end
    return most_recent_quotes.first.cost_per_unit_cents(currency) unless most_recent_quotes.empty?
    0
  end
end
