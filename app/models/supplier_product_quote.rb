# == Schema Information
#
# Table name: supplier_product_quotes
#
#  id                           :uuid             not null, primary key
#  quantity                     :integer
#  state                        :integer
#  total_shipping_cents         :integer
#  unit_price_cents             :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  supplier_product_id          :uuid
#  supplier_contact_id          :uuid
#  supplier_product_shipment_id :uuid
#  currency_id                  :uuid
#  shipping_provider_id         :uuid
#

class SupplierProductQuote < ApplicationRecord
  include AASM

  # States:
  enum state: {
    enquired: 0,
    negotiating: 1,
    ordering: 2,
    waiting_for_tracking_number: 3,
    waiting_for_shipment: 4,
    shipped: 5,
    dropshipping: -1,
    discarded: -2
  }

  aasm column: :state, enum: true do
    state :enquired, initial: true
    state :negotiating
    state :ordering
    state :waiting_for_tracking_number
    state :waiting_for_shipment
    state :received
    state :dropshipping
    state :discarded

    event :negotiate do
      transitions from: :enquired, to: :negotiating, after: Proc.new { supplier_product.product.negotiate! }
    end

    event :order do
      transitions from: :negotiating, to: :ordering, after: Proc.new { supplier_product.product.order! }
    end

    event :pay do
      transitions from: :ordering, to: :waiting_for_tracking_number, after: Proc.new { supplier_product.product.pay! }
    end

    event :wait_for_shipping do
      transitions from: :waiting_for_tracking_number, to: :waiting_for_shipment, after: Proc.new { supplier_product.product.wait_for_shipping! }
    end

    event :receive do
      transitions from: :waiting_for_shipment, to: :received, after: Proc.new { supplier_product.product.receive! }
    end
  end

  # Associations
  belongs_to :currency
  belongs_to :shipping_provider
  belongs_to :supplier_product
  belongs_to :supplier_contact
  belongs_to :supplier_product_shipment, optional: true

  # Validations
  validates :currency, presence: true

  def currency_name
    currency.present? ? currency.name : nil
  end

  def total_cents
    return nil if unit_price_cents.nil? || total_shipping_cents.nil? || quantity.nil?
    unit_price_cents * quantity + total_shipping_cents
  end

  def unit_price
    return 'N/A' if unit_price_cents.nil?
    return '??' if currency_name.nil?
    Money.new(unit_price_cents, currency_name).format(symbol: currency_name + ' ')
  end

  def shipping_price
    return 'N/A' if total_shipping_cents.nil?
    return '??' if currency_name.nil?
    Money.new(total_shipping_cents, currency_name).format(symbol: currency_name + ' ')
  end

  def total_price_in_currency(destination_currency)
    return 'N/A' if total_cents.nil?
    total_cost = total_cents * conversion_rate(currency_name, destination_currency.name)
    Money.new(total_cost, destination_currency.name).format(symbol: destination_currency.name + ' ')
  end

  def total_price
    return 'N/A' if total_cents.nil?
    return '??' if currency_name.nil?
    Money.new(total_cents, currency_name).format(symbol: currency_name + ' ')
  end

  def cost_per_unit_cents(destination_currency)
    return -1 if total_cents.nil?
    return -1 if currency_name.nil?
    (total_cents.to_f / quantity.to_f) * conversion_rate(currency_name, destination_currency.name)
  end

  def cost_per_unit(destination_currency)
    return 'N/A' if total_cents.nil?
    return '??' if currency_name.nil?
    unit_cost = (total_cents.to_f / quantity.to_f) * conversion_rate(currency_name, destination_currency.name)
    Money.new(unit_cost, destination_currency.name).format(symbol: destination_currency.name + ' ')
  end

  # TODO: Move to a use case?
  def conversion_rate(source_currency, destination_currency)
    return 1.32 if source_currency == 'USD' && destination_currency == 'AUD'
    return 1 if source_currency == destination_currency
    0
  end

  def has_shipment?
    supplier_product_shipment.present?
  end

  class << self
    def not_shipped
      where.not(state: 'shipped')
    end

  end
end
