# == Schema Information
#
# Table name: product_reviews
#
#  id             :uuid             not null, primary key
#  reviewer       :string
#  reviewer_email :string
#  review_body    :string
#  source_url     :string
#  rating         :integer
#  date           :datetime
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  product_id     :uuid
#  review_title   :string
#

class ProductReview < ApplicationRecord
  after_initialize :set_due_date

  belongs_to :product
  has_many :product_review_images

  validates :rating, presence: true
  validates :product, presence: true
  validates :reviewer, presence: true
  validates :review_body, presence: true
  validates :source_url, presence: true

  def set_due_date
    date ||= DateTime.now
  end

  def name
    "Review of \'#{product.name}\' by #{reviewer}"
  end
end
