# == Schema Information
#
# Table name: shipping_providers
#
#  id         :uuid             not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ShippingProvider < ApplicationRecord
end
