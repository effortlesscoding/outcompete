class ApiController < ActionController::Base 
  before_action :authenticate_user

  private
  
  def authenticate_user
    user_token = request.headers['X-USER-TOKEN']
    if user_token
      @admin_user = AdminUser.find_by_token(user_token)
      #Unauthorize if a user object is not returned
      if @admin_user.nil?
        return unauthorize
      end
    else
      return unauthorize
    end
  end

  def unauthorize
    head status: :unauthorized
    return false
  end

end