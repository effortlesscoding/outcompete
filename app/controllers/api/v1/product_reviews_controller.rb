module Api::V1
  class ProductReviewsController < ApiController

    def index
      reviews = ProductReview.all
      render json: {status: 'SUCCESS', message: 'Loaded all posts', data: reviews}, status: :ok
    end

    def judgme_csv
      puts params[:product_id]
      reviews = ProductReview.all
      render json: {status: 'SUCCESS CSV', message: 'Loaded all posts', data: reviews}, status: :ok
    end
  end
end