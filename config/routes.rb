Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api do 
    namespace :v1 do 
      get '/product_reviews/judgme_csv', to: 'product_reviews#judgme_csv'
    end 
  end 
  root to: 'admin/dashboard#index'
end
