API_KEY = ENV['OUTCOMPETE_SHOPIFY_API_KEY']
PASSWORD = ENV['OUTCOMPETE_SHOPIFY_PASSWORD']
SHOP_NAME = ENV['OUTCOMPETE_SHOPIFY_NAME']
# TODO : Read it from the database so I can use multiple shopify stores at once.
shop_url = "https://#{API_KEY}:#{PASSWORD}@#{SHOP_NAME}.myshopify.com/admin"
ShopifyAPI::Base.site = shop_url
