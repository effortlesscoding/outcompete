class AddReferenceNumberToFactoryContacts < ActiveRecord::Migration[5.1]
  def change
    add_column :factory_contacts, :reference_number, :string
  end
end
