class RenameFKs < ActiveRecord::Migration[5.1]
  def change
    rename_column :supplier_contacts, :factory_id, :supplier_id
    rename_column :supplier_products, :factory_id, :supplier_id
    rename_column :supplier_product_quotes, :vendor_product_id, :supplier_product_id
    rename_column :supplier_product_quotes, :factory_contact_id, :supplier_contact_id
  end
end
