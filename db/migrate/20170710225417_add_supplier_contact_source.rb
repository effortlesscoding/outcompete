class AddSupplierContactSource < ActiveRecord::Migration[5.1]
  def change
    create_table :contact_sources, id: :uuid do |t|
      t.string :name
      t.timestamps
    end
    add_reference :supplier_contacts, :contact_source, type: :uuid, foreign_key: true
  end
end
