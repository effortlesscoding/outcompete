class CreateSupplierProductShipments < ActiveRecord::Migration[5.1]
  def change
    create_table :supplier_product_shipments, id: :uuid do |t|
      t.string :shipping_provider
      t.string :tracking_number
      t.string :tracking_url
      
      t.timestamps
    end
  end
end
