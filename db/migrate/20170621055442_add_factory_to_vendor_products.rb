class AddFactoryToVendorProducts < ActiveRecord::Migration[5.1]
  def change
    add_reference :vendor_products, :factory, type: :uuid, foreign_key: true
  end
end
