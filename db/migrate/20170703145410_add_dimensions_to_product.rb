class AddDimensionsToProduct < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :width_mm, :integer
    add_column :products, :height_mm, :integer
    add_column :products, :depth_mm, :integer
    add_column :products, :weight_gram, :integer
  end
end
