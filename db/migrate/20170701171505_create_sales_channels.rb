class CreateSalesChannels < ActiveRecord::Migration[5.1]
  def change
    create_table :sales_channels, id: :uuid do |t|
      t.string :name
      t.string :url
      t.timestamps
    end
  end
end
