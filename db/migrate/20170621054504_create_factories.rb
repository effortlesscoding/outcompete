class CreateFactories < ActiveRecord::Migration[5.1]
  def change
    create_table :factories, id: :uuid do |t|
      t.string :name
      t.string :source
      t.string :location
      t.string :profile_url

      t.timestamps
    end
  end
end
