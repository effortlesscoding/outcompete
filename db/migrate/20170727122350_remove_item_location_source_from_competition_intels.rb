class RemoveItemLocationSourceFromCompetitionIntels < ActiveRecord::Migration[5.1]
  def change
    remove_column :competitor_product_intels, :item_location
    remove_column :competitor_product_intels, :source
  end
end
