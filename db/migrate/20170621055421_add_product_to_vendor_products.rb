class AddProductToVendorProducts < ActiveRecord::Migration[5.1]
  def change
    add_reference :vendor_products, :product, type: :uuid, foreign_key: true
  end
end
