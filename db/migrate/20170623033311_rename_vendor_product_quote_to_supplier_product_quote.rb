class RenameVendorProductQuoteToSupplierProductQuote < ActiveRecord::Migration[5.1]
  def change
    rename_table :vendor_product_quotes, :supplier_product_quotes
  end
end
