class RenameFactoryToSupplier < ActiveRecord::Migration[5.1]
  def change
    rename_table :factories, :suppliers
  end
end
