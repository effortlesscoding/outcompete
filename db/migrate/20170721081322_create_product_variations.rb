class CreateProductVariations < ActiveRecord::Migration[5.1]
  def change
    create_table :product_variations, id: :uuid do |t|
      t.string :name
      t.timestamps
    end
  end
end
