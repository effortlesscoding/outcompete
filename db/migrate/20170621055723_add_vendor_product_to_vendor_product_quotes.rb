class AddVendorProductToVendorProductQuotes < ActiveRecord::Migration[5.1]
  def change
    add_reference :vendor_product_quotes, :vendor_product, type: :uuid, foreign_key: true
  end
end
