class AddProductToCompetitorProductIntels < ActiveRecord::Migration[5.1]
  def change
    add_reference :competitor_product_intels, :product, type: :uuid, foreign_key: true
  end
end
