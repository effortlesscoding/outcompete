class AddCurrencyReferenceToCompetitorIntels < ActiveRecord::Migration[5.1]
  def change
    add_reference :competitor_product_intels, :currency, type: :uuid, foreign_key: true
  end
end
