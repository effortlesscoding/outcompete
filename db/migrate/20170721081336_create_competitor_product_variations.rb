class CreateCompetitorProductVariations < ActiveRecord::Migration[5.1]
  def change
    create_table :competitor_product_variations, id: :uuid do |t|
      t.string :value
      t.timestamps
    end
    add_reference :competitor_product_variations, :product_variation, type: :uuid, foreign_key: true
  end
end
