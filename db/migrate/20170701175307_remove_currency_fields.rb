class RemoveCurrencyFields < ActiveRecord::Migration[5.1]
  def change
    remove_column :competitor_product_intels, :currency, type: :string
    remove_column :supplier_product_quotes, :currency, type: :string
  end
end
