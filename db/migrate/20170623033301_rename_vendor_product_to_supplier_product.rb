class RenameVendorProductToSupplierProduct < ActiveRecord::Migration[5.1]
  def change
    rename_table :vendor_products, :supplier_products
  end
end
