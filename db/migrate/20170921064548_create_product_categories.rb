class CreateProductCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :product_categories, id: :uuid do |t|
      t.string :name
      t.timestamps
    end
    add_reference :products, :product_category, type: :uuid, foreign_key: true
  end
end
