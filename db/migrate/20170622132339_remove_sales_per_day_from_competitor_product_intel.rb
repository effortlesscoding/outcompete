class RemoveSalesPerDayFromCompetitorProductIntel < ActiveRecord::Migration[5.1]
  def change
    remove_column :competitor_product_intels, :sales_per_day
  end
end
