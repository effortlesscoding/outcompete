class AddSourceReferenceToSuppliers < ActiveRecord::Migration[5.1]
  def change
    add_reference :suppliers, :supplier_source, type: :uuid, foreign_key: true
  end
end
