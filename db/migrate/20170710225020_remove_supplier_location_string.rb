class RemoveSupplierLocationString < ActiveRecord::Migration[5.1]
  def change
    remove_column :supplier_product_quotes, :source, type: :string
    remove_column :suppliers, :location, type: :string
  end
end
