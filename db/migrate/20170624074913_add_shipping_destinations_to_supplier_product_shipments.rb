class AddShippingDestinationsToSupplierProductShipments < ActiveRecord::Migration[5.1]
  def change
    add_reference :supplier_product_shipments, :shipping_destination, type: :uuid, foreign_key: true
  end
end
