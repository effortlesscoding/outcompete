class CreateFactoryContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :factory_contacts, id: :uuid do |t|
      t.string :name
      t.string :contact_type
      t.string :contact_id
      
      t.timestamps
    end
  end
end
