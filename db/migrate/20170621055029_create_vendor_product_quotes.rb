class CreateVendorProductQuotes < ActiveRecord::Migration[5.1]
  def change
    create_table :vendor_product_quotes, id: :uuid do |t|
      t.integer :quantity
      t.integer :state
      t.integer :total_shipping_cents
      t.integer :unit_price_cents
      t.string :currency

      t.timestamps
    end
  end
end
