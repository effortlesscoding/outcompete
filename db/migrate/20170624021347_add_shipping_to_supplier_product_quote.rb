class AddShippingToSupplierProductQuote < ActiveRecord::Migration[5.1]
  def change
    add_reference :supplier_product_quotes, :supplier_product_shipment, type: :uuid, foreign_key: true
  end
end
