class AddUrlToSupplierProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :supplier_products, :reference_url, :string
  end
end
