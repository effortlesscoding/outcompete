class CreateShippingDestinations < ActiveRecord::Migration[5.1]
  def change
    create_table :shipping_destinations, id: :uuid do |t|
      t.string :name
      t.string :phone
      t.string :address

      t.timestamps
    end
  end
end
