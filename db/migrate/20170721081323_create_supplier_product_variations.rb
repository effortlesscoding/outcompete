class CreateSupplierProductVariations < ActiveRecord::Migration[5.1]
  def change
    create_table :supplier_product_variations, id: :uuid do |t|
      t.string :value
      t.timestamps
    end
    add_reference :supplier_product_variations, :product_variation, type: :uuid, foreign_key: true
    add_reference :supplier_product_variations, :supplier_product, type: :uuid, foreign_key: true
  end
end
