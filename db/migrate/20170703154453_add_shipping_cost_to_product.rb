class AddShippingCostToProduct < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :shipping_cost_cents, :integer
  end
end
