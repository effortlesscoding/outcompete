class AddCurrencyReferenceToQuotes < ActiveRecord::Migration[5.1]
  def change
    add_reference :supplier_product_quotes, :currency, type: :uuid, foreign_key: true
  end
end
