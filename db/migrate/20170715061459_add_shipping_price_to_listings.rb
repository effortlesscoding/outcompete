class AddShippingPriceToListings < ActiveRecord::Migration[5.1]
  def change
    add_column :product_listings, :shipping_price_cents, :integer
  end
end
