class AddPrivilegeLevelToAdminUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :admin_users, :privilege_level, :integer
  end
end
