class RenameFactoryContactToSupplierContact < ActiveRecord::Migration[5.1]
  def change
    rename_table :factory_contacts, :supplier_contacts
  end
end
