class CreateShippingProviders < ActiveRecord::Migration[5.1]
  def change
    create_table :shipping_providers, id: :uuid do |t|
      t.string :name
      t.timestamps
    end
  end
end
