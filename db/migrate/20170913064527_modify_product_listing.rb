class ModifyProductListing < ActiveRecord::Migration[5.1]
  def change
    add_column :product_listings, :state, :integer
    add_column :product_listings, :error_message, :string
    rename_column :product_listings, :address, :url
  end
end
