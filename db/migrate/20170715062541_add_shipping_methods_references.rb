class AddShippingMethodsReferences < ActiveRecord::Migration[5.1]
  def change
    add_reference :supplier_product_shipments, :shipping_provider, type: :uuid, foreign_key: true
    add_reference :supplier_product_quotes, :shipping_provider, type: :uuid, foreign_key: true
  end
end
