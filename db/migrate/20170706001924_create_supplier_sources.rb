class CreateSupplierSources < ActiveRecord::Migration[5.1]
  def change
    create_table :supplier_sources, id: :uuid do |t|
      t.string :name
      t.string :url
      t.timestamps
    end
  end
end
