class CreateVendorProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :vendor_products, id: :uuid do |t|
      t.string :name
      t.string :image_url
      t.string :description
      t.integer :quantity_in_stock

      t.timestamps
    end
  end
end
