class CreateCompetitorProductIntels < ActiveRecord::Migration[5.1]
  def change
    create_table :competitor_product_intels, id: :uuid do |t|
      t.integer :days_listed
      t.integer :sales_per_day
      t.integer :shipping_price_cents
      t.integer :total_sales
      t.integer :unit_price_cents
      t.string :additional_info
      t.string :currency
      t.string :image_url
      t.string :item_location
      t.string :source
      t.string :url
      t.timestamp :intel_date

      t.timestamps
    end
  end
end
