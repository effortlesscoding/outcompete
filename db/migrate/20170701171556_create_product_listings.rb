class CreateProductListings < ActiveRecord::Migration[5.1]
  def change
    create_table :product_listings, id: :uuid do |t|
      t.string :name
      t.string :phone
      t.string :address
      t.integer :listing_costs_cents
      t.integer :listing_price_cents
      t.timestamp :start_date
      t.timestamp :end_date
      t.timestamps
    end
    add_reference :product_listings, :currency, type: :uuid, foreign_key: true
    add_reference :product_listings, :sales_channel, type: :uuid, foreign_key: true
    add_reference :product_listings, :product, type: :uuid, foreign_key: true
  end
end
