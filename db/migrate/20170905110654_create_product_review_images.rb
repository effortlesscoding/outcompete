class CreateProductReviewImages < ActiveRecord::Migration[5.1]
  def change
    create_table :product_review_images, id: :uuid do |t|
      t.string :image_url
      t.timestamps
    end
    add_reference :product_review_images, :product_review, type: :uuid, foreign_key: true
  end
end
