class AddFactoryToFactoryContacts < ActiveRecord::Migration[5.1]
  def change
    add_reference :factory_contacts, :factory, type: :uuid, foreign_key: true
  end
end
