class AddMarketingMaterialsUrlToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :marketing_materials_url, :string
  end
end
