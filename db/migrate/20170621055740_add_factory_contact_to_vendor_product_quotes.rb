class AddFactoryContactToVendorProductQuotes < ActiveRecord::Migration[5.1]
  def change
    add_reference :vendor_product_quotes, :factory_contact, type: :uuid, foreign_key: true
  end
end
