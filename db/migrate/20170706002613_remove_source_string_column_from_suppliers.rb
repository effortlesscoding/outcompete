class RemoveSourceStringColumnFromSuppliers < ActiveRecord::Migration[5.1]
  def change
    remove_column :suppliers, :source, type: :string
  end
end
