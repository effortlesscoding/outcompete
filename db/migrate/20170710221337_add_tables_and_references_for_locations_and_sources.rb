class AddTablesAndReferencesForLocationsAndSources < ActiveRecord::Migration[5.1]
  def change
    create_table :locations, id: :uuid do |t|
      t.string :name
      t.timestamps
    end
    add_reference :suppliers, :location, type: :uuid, foreign_key: true
    add_reference :competitor_product_intels, :location, type: :uuid, foreign_key: true
    add_reference :competitor_product_intels, :sales_channel, type: :uuid, foreign_key: true
  end
end
