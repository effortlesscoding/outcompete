class CreateProductReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :product_reviews, id: :uuid do |t|
      t.string :reviewer
      t.string :reviewer_email
      t.string :review_title
      t.string :review_body
      t.string :source_url
      t.integer :rating
      t.timestamp :date, default: -> { 'CURRENT_TIMESTAMP' }
      t.timestamps
    end
    add_reference :product_reviews, :product, type: :uuid, foreign_key: true
  end
end
