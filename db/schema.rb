# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170921064548) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pgcrypto"

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "privilege_level"
    t.string "token"
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "competitor_product_intels", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "days_listed"
    t.integer "shipping_price_cents"
    t.integer "total_sales"
    t.integer "unit_price_cents"
    t.string "additional_info"
    t.string "image_url"
    t.string "url"
    t.datetime "intel_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "product_id"
    t.uuid "currency_id"
    t.integer "total_entries"
    t.uuid "location_id"
    t.uuid "sales_channel_id"
    t.index ["currency_id"], name: "index_competitor_product_intels_on_currency_id"
    t.index ["location_id"], name: "index_competitor_product_intels_on_location_id"
    t.index ["product_id"], name: "index_competitor_product_intels_on_product_id"
    t.index ["sales_channel_id"], name: "index_competitor_product_intels_on_sales_channel_id"
  end

  create_table "competitor_product_variations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "product_variation_id"
    t.index ["product_variation_id"], name: "index_competitor_product_variations_on_product_variation_id"
  end

  create_table "contact_sources", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "currencies", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "locations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_categories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_listings", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.string "url"
    t.integer "listing_costs_cents"
    t.integer "listing_price_cents"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "currency_id"
    t.uuid "sales_channel_id"
    t.uuid "product_id"
    t.integer "shipping_price_cents"
    t.integer "state"
    t.string "error_message"
    t.index ["currency_id"], name: "index_product_listings_on_currency_id"
    t.index ["product_id"], name: "index_product_listings_on_product_id"
    t.index ["sales_channel_id"], name: "index_product_listings_on_sales_channel_id"
  end

  create_table "product_review_images", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "image_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "product_review_id"
    t.index ["product_review_id"], name: "index_product_review_images_on_product_review_id"
  end

  create_table "product_reviews", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "reviewer"
    t.string "reviewer_email"
    t.string "review_body"
    t.string "source_url"
    t.integer "rating"
    t.datetime "date", default: -> { "now()" }
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "product_id"
    t.string "review_title"
    t.index ["product_id"], name: "index_product_reviews_on_product_id"
  end

  create_table "product_variations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "products", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "image_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "state"
    t.string "marketing_materials_url"
    t.integer "width_mm"
    t.integer "height_mm"
    t.integer "depth_mm"
    t.integer "weight_gram"
    t.integer "shipping_cost_cents"
    t.string "code"
    t.uuid "product_category_id"
    t.index ["product_category_id"], name: "index_products_on_product_category_id"
  end

  create_table "sales_channels", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "scripts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shipping_destinations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.string "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shipping_providers", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "supplier_contacts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "contact_type"
    t.string "contact_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "supplier_id"
    t.string "reference_number"
    t.uuid "contact_source_id"
    t.index ["contact_source_id"], name: "index_supplier_contacts_on_contact_source_id"
    t.index ["supplier_id"], name: "index_supplier_contacts_on_supplier_id"
  end

  create_table "supplier_product_quotes", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "quantity"
    t.integer "state"
    t.integer "total_shipping_cents"
    t.integer "unit_price_cents"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "supplier_product_id"
    t.uuid "supplier_contact_id"
    t.uuid "supplier_product_shipment_id"
    t.uuid "currency_id"
    t.uuid "shipping_provider_id"
    t.index ["currency_id"], name: "index_supplier_product_quotes_on_currency_id"
    t.index ["shipping_provider_id"], name: "index_supplier_product_quotes_on_shipping_provider_id"
    t.index ["supplier_contact_id"], name: "index_supplier_product_quotes_on_supplier_contact_id"
    t.index ["supplier_product_id"], name: "index_supplier_product_quotes_on_supplier_product_id"
    t.index ["supplier_product_shipment_id"], name: "index_supplier_product_quotes_on_supplier_product_shipment_id"
  end

  create_table "supplier_product_shipments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "shipping_provider"
    t.string "tracking_number"
    t.string "tracking_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "shipping_destination_id"
    t.uuid "shipping_provider_id"
    t.index ["shipping_destination_id"], name: "index_supplier_product_shipments_on_shipping_destination_id"
    t.index ["shipping_provider_id"], name: "index_supplier_product_shipments_on_shipping_provider_id"
  end

  create_table "supplier_product_variations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "product_variation_id"
    t.uuid "supplier_product_id"
    t.index ["product_variation_id"], name: "index_supplier_product_variations_on_product_variation_id"
    t.index ["supplier_product_id"], name: "index_supplier_product_variations_on_supplier_product_id"
  end

  create_table "supplier_products", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "image_url"
    t.string "description"
    t.integer "quantity_in_stock"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "product_id"
    t.uuid "supplier_id"
    t.string "reference_url"
    t.index ["product_id"], name: "index_supplier_products_on_product_id"
    t.index ["supplier_id"], name: "index_supplier_products_on_supplier_id"
  end

  create_table "supplier_sources", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "suppliers", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "profile_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "supplier_source_id"
    t.uuid "location_id"
    t.index ["location_id"], name: "index_suppliers_on_location_id"
    t.index ["supplier_source_id"], name: "index_suppliers_on_supplier_source_id"
  end

  add_foreign_key "competitor_product_intels", "currencies"
  add_foreign_key "competitor_product_intels", "locations"
  add_foreign_key "competitor_product_intels", "products"
  add_foreign_key "competitor_product_intels", "sales_channels"
  add_foreign_key "competitor_product_variations", "product_variations"
  add_foreign_key "product_listings", "currencies"
  add_foreign_key "product_listings", "products"
  add_foreign_key "product_listings", "sales_channels"
  add_foreign_key "product_review_images", "product_reviews"
  add_foreign_key "product_reviews", "products"
  add_foreign_key "products", "product_categories"
  add_foreign_key "supplier_contacts", "contact_sources"
  add_foreign_key "supplier_contacts", "suppliers"
  add_foreign_key "supplier_product_quotes", "currencies"
  add_foreign_key "supplier_product_quotes", "shipping_providers"
  add_foreign_key "supplier_product_quotes", "supplier_contacts"
  add_foreign_key "supplier_product_quotes", "supplier_product_shipments"
  add_foreign_key "supplier_product_quotes", "supplier_products"
  add_foreign_key "supplier_product_shipments", "shipping_destinations"
  add_foreign_key "supplier_product_shipments", "shipping_providers"
  add_foreign_key "supplier_product_variations", "product_variations"
  add_foreign_key "supplier_product_variations", "supplier_products"
  add_foreign_key "supplier_products", "products"
  add_foreign_key "supplier_products", "suppliers"
  add_foreign_key "suppliers", "locations"
  add_foreign_key "suppliers", "supplier_sources"
end
