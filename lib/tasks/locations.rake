namespace :currencies do
  desc 'Generates currency references'

  task add_default: [:environment] do
    Location.delete_all
    Location.new(name: 'China').save
    Location.new(name: 'Australia').save
    Location.new(name: 'Hong Kong').save
    Location.new(name: 'Pakistan').save
    Location.new(name: 'United Kingdom').save
    Location.new(name: 'USA').save
  end

  task set_suppliers_locations: [:environment] do
    china = Location.find_by(name: 'China')
    australia = Location.find_by(name: 'Australia')
    uk = Location.find_by(name: 'United Kingdom')
    usa = Location.find_by(name: 'USA')
    ebay_au = Location.find_by(name: 'eBay AU')
    Supplier.all.each do |supplier|
      supplier.location = china
      supplier.save
    end
  end

  task :set_competitors_locations => [:environment] do
    china = Location.find_by(name: 'China')
    australia = Location.find_by(name: 'Australia')
    uk = Location.find_by(name: 'United Kingdom')
    usa = Location.find_by(name: 'USA')
    ebay_au = SalesChannel.find_by(name: 'eBay AU')
    CompetitorProductIntel.all.each do |intel|
      next if intel.item_location.nil?
      item_location = intel.item_location.downcase
      puts "item_location - #{item_location}"
      if item_location.include? 'china'
        puts ' -- It is China'
        intel.location = china
      elsif item_location.include? 'australia'
        puts ' -- It is Australia'
        intel.location = australia
      elsif item_location.include? 'united kingdom'
        puts ' -- It is the UK!'
        intel.location = uk
      elsif item_location.include? 'usa'
        puts ' -- It is the USA!'
        intel.location = usa
      end
      intel.sales_channel = ebay_au
      intel.save.tap { |success| puts "Save result #{success} #{intel.errors.full_messages}"}
    end
  end
end
