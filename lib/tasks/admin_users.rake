namespace :admin_users do
  desc 'Fixing admin users'

  task :fix => [:environment] do
    AdminUser.all.each do |user|
      user.superadmin!
      user.save.tap { |success| puts "User save failed #{user.errors.full_messages}" unless success }
    end
  end
end
