namespace :contact_sources do
  desc 'Generates currency references'

  task :fix => [:environment] do
    email = ContactSource.find_by(name: 'email')
    alibaba = ContactSource.find_by(name: 'Alibaba Chat')
    aliexpress = ContactSource.find_by(name: 'AliExpress Chat')
    SupplierContact.all.each do |supplier_contact|
      if supplier_contact.contact_type.downcase.include? 'alibaba'
        supplier_contact.contact_source = alibaba
      elsif supplier_contact.contact_type.downcase.include? 'aliexpress'
        supplier_contact.contact_source = aliexpress
      end
      supplier_contact.save
    end
  end
end
