namespace :currencies do
  desc 'Generates currency references'

  task :set => [:environment] do
    CompetitorProductIntel.all.each do |intel|
      intel.currency_id = Currency.find_by(name: 'AUD').id
      intel.save
    end

    SupplierProductQuote.all.each do |quote|
      quote.currency_id = Currency.find_by(name: 'USD').id
      quote.save
    end
  end
end
